//
//  ContactoViewController.swift
//  AgendaHype
//
//  Created by PowerX56 on 1/10/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//

import UIKit
import SQLite
import Alamofire

class celuladistritos :UITableViewCell {
    @IBOutlet weak var textodistrito: UILabel!
}

class ContactoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var textonome: UITextField!
    @IBOutlet weak var textoapelido: UITextField!
    @IBOutlet weak var textotelefone: UITextField!
    @IBOutlet weak var botao: UIButton!
    @IBOutlet weak var textolocalidade: UILabel!
    @IBOutlet weak var tabela: UITableView!
    
    var recebecontactoparaeditar:contacto?
    var editarvalido : Int = 0
    
    var arrayResultet:Array<AnyObject>?
    var quantidade:Int = 0
    var ArrayDicionario:[(titulo: String, body: String)] = []
    var localidadereal : String = ""
    var statsfile:plistclasse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statsfile = plistclasse()
        statsfile?.loadstats()
        view.backgroundColor = statsfile?.ativarcor()
        tabela.backgroundColor = statsfile?.ativarcor()
        
        // Do any additional setup after loading the view.
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 5
        manager.session.configuration.timeoutIntervalForResource = 5
        
        manager.request("https://httpbin.org/get").responseString { response in
            print("Success: \(response.result.isSuccess)")
            if response.result.isSuccess == true{
                let url = URL(string: "http://centraldedados.pt/distritos.json")
                let data = try? Data(contentsOf: url!)
                self.arrayResultet = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! Array<AnyObject>
                for d in self.arrayResultet! {
                    let idd = d["cod_distrito"]
                    let nomed = d["nome_distrito"]
                    self.ArrayDicionario.append((titulo: idd as! String, body: nomed as! String))
                    //print(d)
                    self.quantidade = self.quantidade + 1
                }
                self.tabela.reloadData()
            }else{
                repeat{
                    //1. Create the alert controller.
                    let alert = UIAlertController(title: "Sem internet", message: "Insira a localidade", preferredStyle: .alert)
 
                    alert.addTextField { (textField) in
                        textField.text = "Localidade..."
                    }
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                        let textFielddab = alert?.textFields![0]
                        self.localidadereal = (textFielddab?.text!)!
                    }))
                    self.present(alert, animated: true, completion: nil)
                }while self.localidadereal == ""
                self.textolocalidade.text = "localidade: " + self.localidadereal
            }
        }
        
        if editarvalido == 1 {
            textonome.text = recebecontactoparaeditar?.nomeget
            textoapelido.text = recebecontactoparaeditar?.apelidoget
            textotelefone.text = String(describing: recebecontactoparaeditar!.telefoneget)
            localidadereal = (recebecontactoparaeditar?.localidadeget)!
            textolocalidade.text = "localidade: " + localidadereal
            botao.setTitle("editar", for: .normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addclick(_ sender: Any) {
        if textonome.text != "" && textoapelido.text != "" && textotelefone.text != "" && localidadereal != "" {
            if editarvalido == 0 {
                let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
                print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
                let db = try! Connection(fullPath.absoluteString)
                
                let tabela = Table("contatos")
                //let id = Expression<Int64>("id")
                let nome = Expression<String>("nome")
                let apelido = Expression<String>("apelido")
                let telemovel = Expression<String>("telemovel")
                let localidade = Expression<String>("localidade")
                
                let q1 = tabela.insert(nome <- textonome.text! ,apelido <-  textoapelido.text!, telemovel <- String(textotelefone.text!),localidade <-  localidadereal)
                try! db.run(q1)
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            }else if editarvalido == 1 {
                
                let valordoid : Int64 = Int64((recebecontactoparaeditar?.idget)!)
                
                let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
                print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
                let db = try! Connection(fullPath.absoluteString)
                
                let tabela = Table("contatos")
                let id = Expression<Int64>("id")
                let nome = Expression<String>("nome")
                let apelido = Expression<String>("apelido")
                let telemovel = Expression<String>("telemovel")
                let localidade = Expression<String>("localidade")
              
                let ze = tabela.filter(id == valordoid)
                try! db.run(ze.update(nome <- textonome.text!, apelido <-  textoapelido.text!, telemovel <- textotelefone.text!,localidade <- localidadereal))
                
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            }
        }
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quantidade
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celula2") as? celuladistritos
        cell!.textodistrito.text = ArrayDicionario[indexPath.row].body
        cell?.backgroundColor = statsfile?.ativarcor()
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! celuladistritos
        localidadereal = currentCell.textodistrito!.text!
        textolocalidade.text = "localidade: " + localidadereal
    }
    
}
