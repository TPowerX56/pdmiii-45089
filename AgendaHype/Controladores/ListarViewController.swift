//
//  ListarViewController.swift
//  AgendaHype
//
//  Created by PowerX56 on 1/10/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//

import UIKit
import SQLite

class lembretes: UITableViewCell {
    @IBOutlet weak var textotitulo: UILabel!
    @IBOutlet weak var botaoadd: UIButton!
    
}

class ListarViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var recebecontacto:contacto?
    var statsfile:plistclasse?
    var lembretegroup:lembretepack?
    var sendstat: Int = 0
    var idcontactoultra : Int = 0
    var notaenvio : nota?
    
    @IBOutlet weak var tabelalembrete: UITableView!
    
    @IBOutlet weak var nometextoshow: UILabel!
    @IBOutlet weak var contactotextoshow: UILabel!
    @IBOutlet weak var localidadeshow: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statsfile = plistclasse()
        statsfile?.loadstats()
        notaenvio = nota()
        view.backgroundColor = statsfile?.ativarcor()
        lembretegroup = lembretepack()
        let idshadow = Int64((recebecontacto?.idget)!)
        lembretegroup?.criarcarregartabela(idcontactorecebe: Int(idshadow))
        tabelalembrete.backgroundColor = statsfile?.ativarcor()
        
        let RightButton = UIBarButtonItem(title: "Remover", style: .plain, target: self, action: #selector(self.someFunc))
        self.navigationItem.rightBarButtonItem = RightButton
        
        showdatanow()
    }
    
    func showdatanow() {
        nometextoshow.text = recebecontacto?.nomeCompleto
        contactotextoshow.text = String(describing: self.recebecontacto!.tlm)
        localidadeshow.text = recebecontacto?.localidadeget
    }

    @objc func someFunc() {
        let valordoid : Int64 = Int64((recebecontacto?.idget)!)

        let refreshAlert = UIAlertController(title: "Eliminar", message: "Este contacto sera eliminado", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
            print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
            
            let db = try! Connection(fullPath.absoluteString)
            
            let users = Table("contatos")
            let id = Expression<Int64>("id")
            
            self.lembretegroup?.deletepelocontacto(idcontactodelete: valordoid)
            
            let del = users.filter(id == valordoid)
            try! db.run(del.delete())
            
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func editarbtt(_ sender: Any) {
        performSegue(withIdentifier: "editar", sender: recebecontacto)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let d = segue.destination as? ContactoViewController {
            if let s = sender as? contacto {
                d.recebecontactoparaeditar = s
                d.editarvalido = 1
            }
        }else if let d = segue.destination as? NotaViewController {
            if let s = sender as? nota {
                d.recebenota = s
                d.addoueditar = sendstat
                d.recebeidcontacto = idcontactoultra
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let idshadow = Int64((recebecontacto?.idget)!)
        
        let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
        
        let db = try! Connection(fullPath.absoluteString)
        let users = Table("contatos")
        let id = Expression<Int64>("id")
        let nome = Expression<String>("nome")
        let apelido = Expression<String>("apelido")
        let telemovel = Expression<String>("telemovel")
        let localidade = Expression<String>("localidade")
        
        let obtercontacto = users.filter(id == idshadow)
        for user in try! db.prepare(obtercontacto){
            recebecontacto = contacto(idcontacto: Int(idshadow), nome: String(describing: user[nome]), apelido: String(describing: user[apelido]), tlm: Int(String(describing: user[telemovel]))!, localidade: String(describing: user[localidade]))
        }
        showdatanow()
        lembretegroup?.criarcarregartabela(idcontactorecebe: Int(idshadow))
        tabelalembrete.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (lembretegroup?.count)! + 1
    }
    
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celulalembrete") as! lembretes
        if indexPath.row < (lembretegroup?.count)!{
            cell.textotitulo.isHidden = false
            cell.textotitulo.text = lembretegroup?.getNota(index: indexPath.row).tituloget
            cell.botaoadd.isHidden = true
        }else{
            cell.textotitulo.isHidden = true
            cell.botaoadd.isHidden = false
        }
        cell.backgroundColor = statsfile?.ativarcor()
        return cell
    }
    
    
    @IBAction func botaonovanota(_ sender: Any) {
        sendstat = 0
        idcontactoultra = (recebecontacto?.idget)!
        performSegue(withIdentifier: "notasegue", sender: notaenvio)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row <= (lembretegroup?.count)! {
            sendstat = 1
            idcontactoultra = 0
            notaenvio = lembretegroup?.getNota(index: indexPath.row)
            performSegue(withIdentifier: "notasegue", sender: notaenvio)
        }
    }
    
}
