//
//  NotaViewController.swift
//  AgendaHype
//
//  Created by PowerX56 on 2/18/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//

import UIKit
import SQLite

class NotaViewController: UIViewController {

    @IBOutlet weak var addtitle: UILabel!
    @IBOutlet weak var titulotexto: UITextField!
    @IBOutlet weak var conteudotexto: UITextView!
    
    @IBOutlet weak var botaoadd: UIButton!
    
    var statsfile:plistclasse?
    var recebenota : nota?
    var addoueditar: Int = 0
    var lembretegroup:lembretepack?
    var recebeidcontacto : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lembretegroup = lembretepack()
        conteudotexto!.layer.borderWidth = 1
        conteudotexto!.layer.borderColor = UIColor.black.cgColor
        statsfile = plistclasse()
        statsfile?.loadstats()
        view.backgroundColor = statsfile?.ativarcor()
        
        if addoueditar == 0 {
            addtitle.text = "Adicionar Lembrete"
            botaoadd.setTitle("Adicionar", for: .normal)
        }else if addoueditar == 1 {
            titulotexto.text = recebenota?.tituloget
            conteudotexto.text = recebenota?.conteudoget
            addtitle.text = "Editar Lembrete"
            botaoadd.setTitle("Editar", for: .normal)
            let RightButton = UIBarButtonItem(title: "Remover", style: .plain, target: self, action: #selector(self.someFunc))
            self.navigationItem.rightBarButtonItem = RightButton
        }
    }

    @objc func someFunc() {
        let valoridnota : Int = (recebenota?.idnotaget)!
        let valordoid : Int64 = Int64(valoridnota)
        
        let refreshAlert = UIAlertController(title: "Eliminar", message: "Esta nota sera eliminada", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
            print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
            
            let db = try! Connection(fullPath.absoluteString)
            
            let users = Table("lembrete")
            let id = Expression<Int64>("idlembrete")
            
            let del = users.filter(id == valordoid)
            try! db.run(del.delete())
            
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
  
    }
    
    @IBAction func botaoaddpress(_ sender: Any) {
        if titulotexto.text != "" && conteudotexto.text != "" {
            if self.addoueditar == 0 {
                lembretegroup?.addnovanota(idcontactoadd: recebeidcontacto, titulotexto: titulotexto.text!, conteudotexto: conteudotexto.text)
            }else if self.addoueditar == 1 {
                lembretegroup?.editarnota(idnotaeditar: (recebenota?.idnotaget)!, titulotexto: titulotexto.text!, conteudotexto: conteudotexto.text)
            }
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
    }
    

}
