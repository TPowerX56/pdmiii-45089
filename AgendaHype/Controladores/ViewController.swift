//
//  ViewController.swift
//  AgendaHype
//
//  Created by PowerX56 on 1/10/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//


import UIKit
import SQLite

class ViewController: UITableViewController,UISearchBarDelegate{

    @IBOutlet weak var pesquisa: UISearchBar!    
    @IBOutlet var tabelareal: UITableView!
    var agendacontactos:agenda?
    var agendafiltrada:agenda?
    var pesquisando = false
    var queryinject = 0
    var statsfile:plistclasse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statsfile = plistclasse()
        statsfile?.loadstats()
        view.backgroundColor = statsfile?.ativarcor()
        tabelareal.backgroundColor = statsfile?.ativarcor()
        getdata()
        pesquisa.delegate = self
        pesquisa.returnKeyType = UIReturnKeyType.done
    }
    
    func getdata()  {
        let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
        print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
        let db = try! Connection(fullPath.absoluteString)
        
        let tabela = Table("contatos")
        let id = Expression<Int64>("id")
        let nome = Expression<String>("nome")
        let apelido = Expression<String>("apelido")
        let telemovel = Expression<String>("telemovel")
        let localidade = Expression<String>("localidade")
        
        do {
            try db.run(tabela.create { t in
                
                t.column(id, primaryKey: true)
                t.column(nome)
                t.column(apelido)
                t.column(telemovel)
                t.column(localidade)
            })
            
        }catch{
            
        }
        
        agendacontactos = agenda()
        agendafiltrada = agenda()
        
        if queryinject == 0  {
            for user in try! db.prepare(tabela){
                agendacontactos?.addContact(idcontacto: Int(user[id]), nome: user[nome], apelido: user[apelido], tlm: Int(user[telemovel])!,localidade: user[localidade])
            }
        }else if queryinject == 1 {
            if statsfile?.devolvesearchstat() == 0 {
                let query1 = tabela.filter(nome.lowercaseString.like("%" + (pesquisa.text?.lowercased())! + "%"))
                for user in try! db.prepare(query1){
                    agendafiltrada?.addContact(idcontacto: Int(user[id]), nome: user[nome], apelido: user[apelido], tlm: Int(user[telemovel])!,localidade: user[localidade])
                }
            }else if statsfile?.devolvesearchstat() == 1 {
                let query1 = tabela.filter(apelido.lowercaseString.like("%" + (pesquisa.text?.lowercased())! + "%"))
                for user in try! db.prepare(query1){
                    agendafiltrada?.addContact(idcontacto: Int(user[id]), nome: user[nome], apelido: user[apelido], tlm: Int(user[telemovel])!,localidade: user[localidade])
                }
            }else if statsfile?.devolvesearchstat() == 2 {
                let query1 = tabela.filter(telemovel.lowercaseString.like("%" + (pesquisa.text?.lowercased())! + "%"))
                for user in try! db.prepare(query1){
                    agendafiltrada?.addContact(idcontacto: Int(user[id]), nome: user[nome], apelido: user[apelido], tlm: Int(user[telemovel])!,localidade: user[localidade])
                }
            }else if statsfile?.devolvesearchstat() == 3 {
                let query1 = tabela.filter(localidade.lowercaseString.like("%" + (pesquisa.text?.lowercased())! + "%"))
                for user in try! db.prepare(query1){
                    agendafiltrada?.addContact(idcontacto: Int(user[id]), nome: user[nome], apelido: user[apelido], tlm: Int(user[telemovel])!,localidade: user[localidade])
                }
            }

        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pesquisando {
            return agendafiltrada!.count
        }
        return agendacontactos!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celula")
        if statsfile?.devolvecellstat() == 0 {
            if pesquisando {
                cell?.textLabel?.text = agendafiltrada!.getContacto(index: indexPath.row).nomeCompleto
                cell?.imageView?.image = nil
            }else {
                cell?.textLabel?.text = agendacontactos!.getContacto(index: indexPath.row).nomeCompleto
                cell?.imageView?.image = nil
            }
        }else if statsfile?.devolvecellstat() == 1 {
            if pesquisando {
                cell?.textLabel?.text = agendafiltrada!.getContacto(index: indexPath.row).nomeCompleto + ": " + String(agendafiltrada!.getContacto(index: indexPath.row).tlm)
                cell?.imageView?.image = nil
            }else {
                cell?.textLabel?.text = agendacontactos!.getContacto(index: indexPath.row).nomeCompleto + ": " + String(agendacontactos!.getContacto(index: indexPath.row).tlm)
                cell?.imageView?.image = nil
            }
        }else if statsfile?.devolvecellstat() == 2{
            if pesquisando {
                cell?.textLabel?.text = agendafiltrada!.getContacto(index: indexPath.row).nomeCompleto + ": " + String(agendafiltrada!.getContacto(index: indexPath.row).tlm)
                cell?.imageView?.image = #imageLiteral(resourceName: "perfil")
            }else {
                cell?.textLabel?.text = agendacontactos!.getContacto(index: indexPath.row).nomeCompleto + ": " + String(agendacontactos!.getContacto(index: indexPath.row).tlm)
                cell?.imageView?.image = #imageLiteral(resourceName: "perfil")
            }
        }

        cell?.backgroundColor = statsfile?.ativarcor()
        return cell!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getdata()
        statsfile?.loadstats()
        view.backgroundColor = statsfile?.ativarcor()
        tabelareal.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if pesquisando {
        performSegue(withIdentifier: "consulta", sender: agendafiltrada?.getContacto(index: indexPath.row))
        }else{
        performSegue(withIdentifier: "consulta", sender: agendacontactos?.getContacto(index: indexPath.row))
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let d = segue.destination as? ListarViewController {
            if let s = sender as? contacto {
                d.recebecontacto = s
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if pesquisa.text == nil || pesquisa.text == "" {
            pesquisando = false
            view.endEditing(true)
            queryinject = 0
            getdata()
            tabelareal.reloadData()
        }else{
            queryinject = 1
            getdata()
            tabelareal.reloadData()
            pesquisando = true
        }
    }
    
}

