//
//  corprojeto.swift
//  AgendaHype
//
//  Created by PowerX56 on 2/7/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//

import UIKit
import SystemConfiguration

extension UIColor{
    class func MudarCor(red: Int, green: Int, blue: Int, alpha: Int) -> UIColor{
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        let newAlpha = CGFloat(alpha)/100
        return UIColor(red: newRed, green: newGreen ,blue: newBlue , alpha: newAlpha)
    }
}

class plistclasse {
    private var red:Int
    private var green:Int
    private var blue:Int
    private var statcell:Int
    private var statsearch:Int
    
    init() {
        self.red = 255
        self.green = 255
        self.blue = 255
        self.statcell = 0
        self.statsearch = 0
    }
    
    init(red:Int,green:Int, blue:Int, cellget:Int,searchget:Int) {
        self.red = red
        self.green = green
        self.blue = blue
        self.statcell = cellget
        self.statsearch = searchget
    }
    
    func devolvered() -> Float {
        return Float(self.red)
    }
    
    func devolvegreen() -> Float {
        return Float(self.green)
    }
    
    func devolveblue() -> Float {
        return Float(self.blue)
    }
    
    func absorvered(red:Float) {
        self.red = Int(red)
    }
    
    func absorvegreen(green:Float) {
        self.green = Int(green)
    }
    
    func absorveblue(blue:Float) {
        self.blue = Int(blue)
    }
    
    func devolvecellstat() -> Int {
        return statcell
    }
    
    func devolvesearchstat() -> Int {
        return statsearch
    }
    
    func setcell(cellnew:Int) {
        self.statcell = cellnew
    }
    
    func setsearch(searchnew:Int) {
        self.statsearch = searchnew
    }
    
    func loadstats() {
        let fm = FileManager.default
        var BasePath = fm.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.allDomainsMask)
        
        print(BasePath[0])
        let FullPath = BasePath[0].appendingPathComponent("demo.plist")
        
        //let dic = NSMutableDictionary(contentsOf: FullPath)
        //print(dic!)
        
        let fileExists = FileManager().fileExists(atPath: FullPath.path)
        
        if fileExists == false {
            
        }else{
            
            let data = try? Data(contentsOf:FullPath)
            let swiftDictionary = try? PropertyListSerialization.propertyList(from: data!, options: [], format: nil) as! [String:Any]
            
            self.red = swiftDictionary?["red"] as! Int
            self.blue = swiftDictionary?["blue"] as! Int
            self.green = swiftDictionary?["green"] as! Int
            self.statcell = swiftDictionary?["cellx"] as! Int
            self.statsearch = swiftDictionary?["searchy"] as! Int
            
            if self.red == 0 && self.blue == 0 && self.green == 0 {
                self.red = 255
                self.blue = 255
                self.green = 255
            }
        }
    }
    
    func ativarcor() -> UIColor {
        return UIColor.MudarCor(red: Int(self.red), green: Int(self.green), blue: Int(self.blue), alpha: Int(100))
    }
    
    func save() {
        
        let fm = FileManager.default
        
        var BasePath = fm.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.allDomainsMask)
        
        print(BasePath[0])
        let FullPath = BasePath[0].appendingPathComponent("demo.plist")
        
        //let dic2 = NSMutableDictionary(contentsOf: FullPath)
        let dic:NSMutableDictionary = ["red":self.red,"blue":self.blue,"green":self.green,"cellx":self.statcell,"searchy":self.statsearch]
        dic.write(to: FullPath, atomically: true)
    }

    
}
