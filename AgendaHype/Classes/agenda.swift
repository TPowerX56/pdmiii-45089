//
//  agenda.swift
//  AgendaHype
//
//  Created by PowerX56 on 1/10/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//

import UIKit

class agenda {
    var cList:Array<contacto>
    
    var count:Int{
        return cList.count
    }
    
    init() {
        cList = []
    }
    
    
    
    func addContact(c:contacto)  {
        cList.append(c)
    }
    
    
    func addContact(idcontacto: Int, nome:String, apelido:String, tlm:Int,localidade:String)  {
        cList.append(contacto(idcontacto: idcontacto, nome: nome, apelido: apelido, tlm: tlm, localidade:localidade))
    }
    
    
    
    func verContacto(nome:String, apelido:String) -> contacto? {
        
        
        for c in cList{
            
            if c.compApelido(nome: apelido) && c.compNome(nome: nome){
                
                return c
            }
        }
        
        return nil
        
    }
    
    func getContacto(index:Int) -> contacto {
        return self.cList[index]
    }
}
