//
//  contacto.swift
//  AgendaHype
//
//  Created by PowerX56 on 1/10/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//

import UIKit

class contacto {
    private var idcontacto:Int
    private var nome:String
    private var apelido:String
    private var tlmHist:Array<Int>
    private var localidade: String
    
    private var _tlm:Int{
        didSet{
            tlmHist.append(oldValue)
            print("o contacto foi alterado")
        }
        willSet{
            print("o contacto vai ser alterado")
        }
    }
    
    
    var tlm:Int{
        set{
            if String(newValue).hasPrefix("351")  {
                self._tlm = newValue;
            } else{
                print("falta o codigo país")
            }
        }
        get{
            return _tlm
        }
    }
    
    var nomeCompleto:String{
        return "\(self.nome) \(self.apelido)"
    }
    
    var idget:Int{
        return self.idcontacto
    }
    
    var localidadeget:String{
        return self.localidade
    }
    
    var nomeget:String{
        return self.nome
    }
    
    var apelidoget:String{
        return self.apelido
    }
    
    var telefoneget:Int{
        return self.tlm
    }
    
    init(idcontacto:Int,nome:String, apelido:String, tlm:Int, localidade:String) {
        
        self.idcontacto = idcontacto
        self.nome = nome
        self.apelido = apelido
        self._tlm = tlm
        self.tlmHist = []
        self.localidade = localidade
    }
    
    
    
    
    
    //func getTlmSufix() -> String{
    //return "...\( String(self._tlm).suffix(3))"
    //}
    
    
    func getContact() -> (nome:String, tlm:Int) {
        
        return (nomeCompleto, tlm)
        
    }
    
    
    
    func verHist(primeiros n:Int){
        
        if n <= tlmHist.count {
            
            for a in 0..<n{
                print(tlmHist[a])
            }
        }else{
            print("nao tem tantos tlm antigos")
        }
    }
    
    
    
    
    func compNome(nome:String) -> Bool{
        
        return nome.lowercased() == self.nome.lowercased()
        
    }
    
    func compApelido(nome:String) -> Bool{
        
        return apelido.lowercased() == self.apelido.lowercased()
    }
}
