//
//  nota.swift
//  AgendaHype
//
//  Created by PowerX56 on 2/18/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//

import UIKit

class nota {
    private var idnota:Int
    private var idcontacto:Int
    private var titulo:String
    private var conteudo: String
    
    var idnotaget:Int{
        return self.idnota
    }
    
    var idcontactoget:Int{
        return self.idcontacto
    }
    
    var tituloget:String{
        return self.titulo
    }
    
    var conteudoget:String{
        return self.conteudo
    }
    
    init(idnota:Int,idcontacto:Int, titulo:String, conteudo:String) {
        
        self.idnota = idnota
        self.idcontacto = idcontacto
        self.titulo = titulo
        self.conteudo = conteudo
    }
    
    init() {
        self.idnota = 0
        self.idcontacto = 0
        self.titulo = ""
        self.conteudo = ""
    }
    
}
