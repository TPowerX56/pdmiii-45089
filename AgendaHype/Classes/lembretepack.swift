//
//  lembretepack.swift
//  AgendaHype
//
//  Created by PowerX56 on 2/18/18.
//  Copyright © 2018 PowerX56. All rights reserved.
//

import SQLite
import UIKit

class lembretepack {
    var nList:Array<nota>
    
    var count:Int{
        return nList.count
    }
    
    init() {
        nList = []
    }
    
    
    
    func addNota(n:nota)  {
        nList.append(n)
    }
    
    
    func addNota(idnota: Int, idcontacto:Int, titulo:String, conteudo:String)  {
        nList.append(nota(idnota: idnota, idcontacto: idcontacto, titulo: titulo, conteudo: conteudo))
    }
    
    func getNota(index:Int) -> nota {
        return self.nList[index]
    }
    
    func criarcarregartabela(idcontactorecebe: Int) {
        nList.removeAll()
        let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
        print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
        let db = try! Connection(fullPath.absoluteString)
        
        let tabela = Table("lembrete")
        let idlembrete = Expression<Int64>("idlembrete")
        let idcontacto = Expression<Int64>("idcontacto")
        let titulo = Expression<String>("titulo")
        let conteudo = Expression<String>("conteudo")
        
        do {
            try db.run(tabela.create { t in
                
                t.column(idlembrete, primaryKey: true)
                t.column(idcontacto)
                t.column(titulo)
                t.column(conteudo)
            })
            
        }catch{
            
        }
        
        let querynota = tabela.filter(idcontacto == Int64(idcontactorecebe))
        for user in try! db.prepare(querynota){
             nList.append(nota(idnota: Int(user[idlembrete]), idcontacto: Int(user[idcontacto]), titulo: user[titulo], conteudo: user[conteudo]))
        }
    }
    
    func addnovanota(idcontactoadd: Int,titulotexto : String,conteudotexto: String) {
        let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
        print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
        let db = try! Connection(fullPath.absoluteString)
        
        let tabela = Table("lembrete")
       
        let idcontacto = Expression<Int64>("idcontacto")
        let titulo = Expression<String>("titulo")
        let conteudo = Expression<String>("conteudo")
        
        let q1 = tabela.insert(idcontacto <- Int64(idcontactoadd) ,titulo <-  titulotexto, conteudo <- conteudotexto)
        try! db.run(q1)
    }
    
    func editarnota(idnotaeditar:Int, titulotexto:String, conteudotexto: String) {
        
        let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
        print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
        let db = try! Connection(fullPath.absoluteString)
        
        let tabela = Table("lembrete")
        
        let idlembrete = Expression<Int64>("idlembrete")
        let titulo = Expression<String>("titulo")
        let conteudo = Expression<String>("conteudo")
        
        let ze = tabela.filter(idlembrete == Int64(idnotaeditar))
        try! db.run(ze.update(titulo <- titulotexto, conteudo <-  conteudotexto))
    }
    
    func deletepelocontacto(idcontactodelete : Int64)  {
        let fullPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent("db.sqlite3")
        print(fullPath) //para ir buscar o diretorio onde vai ficar alojado o ficheiro SQLite
        
        let db = try! Connection(fullPath.absoluteString)
        
        let users = Table("lembrete")
        let idcontacto = Expression<Int64>("idcontacto")
        
        let del = users.filter(idcontacto == idcontactodelete)
        try! db.run(del.delete())
    }
    
}
